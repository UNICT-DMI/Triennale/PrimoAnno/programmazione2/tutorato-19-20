#include <iostream>

using namespace std;

template <typename T>
class Queue {
    private:
        size_t size;
        size_t head, elements_count;
        T* elements;

    public:
        Queue(size_t);

        void enqueue(T);
        T dequeue();

        bool isEmpty();
        bool isFull();

        void print();

        ~Queue();
};

template <typename T>
Queue<T>::Queue(size_t size) {
    this->size = size;

    this->head = 0;
    this->elements_count = 0;

    this->elements = new T[size];
}

template <typename T>
void Queue<T>::enqueue(T value) {
    elements[(head + elements_count) % size] = value;
    ++elements_count;
}

template <typename T>
T Queue<T>::dequeue() {
    T obj = elements[head];

    --elements_count;
    head = (head + 1) % size;

    return obj;
}

template <typename T>
bool Queue<T>::isEmpty() {
    return elements_count == 0;
}

template <typename T>
bool Queue<T>::isFull() {
    return elements_count == size;
}

template <typename T>
void Queue<T>::print() {
    cout << "| ";

    for(int i = 0; i < elements_count; ++i) {
        cout << elements[(head + i) % size] << " ";
    }

    cout << "| Head: " << head << " Elements count: " << elements_count << endl;
}

template <typename T>
Queue<T>::~Queue() {
    delete elements;
}

int main() {
    Queue<int>* queue = new Queue<int>(4);

    queue->enqueue(0);
    queue->enqueue(1);
    queue->enqueue(2);
    queue->enqueue(3);

    queue->print();

    cout << queue->dequeue() << endl;
    cout << queue->dequeue() << endl;

    queue->enqueue(4);
    queue->enqueue(5);

    queue->print();

    cout << queue->dequeue() << endl;
    cout << queue->dequeue() << endl;
    cout << queue->dequeue() << endl;
    cout << queue->dequeue() << endl;

    queue->print();

    return 0;
}