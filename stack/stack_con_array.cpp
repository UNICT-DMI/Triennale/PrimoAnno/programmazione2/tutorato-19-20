#include <iostream>

using namespace std;

template <typename T>
class Stack {
    private:
        size_t size;
        int head_index;
        T* elements;
    
    public:
        Stack(size_t);

        void push(T);
        T pop();

        bool isEmpty();
        bool isFull();

        void print();

        ~Stack();
};

template <typename T>
Stack<T>::Stack(size_t size) {
    this->size = size;
    this->head_index = -1;

    elements = new T[size];
}

template <typename T>
void Stack<T>::push(T element) {
    ++head_index;
    elements[head_index] = element;
}

template <typename T>
T Stack<T>::pop() {
    T obj = elements[head_index]; 
    --head_index;

    return obj;
}

template <typename T>
bool Stack<T>::isEmpty() {
    return head_index == -1;
}

template <typename T>
bool Stack<T>::isFull() {
    return head_index >= int(size - 1);
}

template <typename T>
void Stack<T>::print() {
    cout << "Elements: ";

    for(int i = this->head_index; i >= 0; --i) {
        cout << elements[i] << " ";
    }

    cout << endl;
}

template <typename T>
Stack<T>::~Stack() {
    delete this->elements;
}

int main() {
    Stack<int>* stack = new Stack<int>(3);

    stack->push(3);
    stack->push(5);
    stack->push(7);

    if(!stack->isFull())
        stack->push(11);

    stack->print();

    while(!stack->isEmpty()) 
        cout << stack->pop() << endl;

    delete stack;

    return 0;
}