#include <iostream>

using namespace std;

template <typename T>
class Node {
    private:
        T value;
        Node<T>* next;

    public:
        Node(T);

        T getValue();

        void setNext(Node*);
        Node* getNext();

        ~Node() { }
};

template <typename T>
Node<T>::Node(T value) {
   this->value = value; 

   this->next = NULL;
}

template <typename T>
T Node<T>::getValue() {
    return this->value;
}

template <typename T>
void Node<T>::setNext(Node<T>* next) {
    this->next = next;
}

template <typename T>
Node<T>* Node<T>::getNext() {
    return this->next;
}

template <typename T>
class Stack {
    private:
        Node<T>* head;
    
    public:
        Stack();

        void push(T);
        T pop();

        bool isEmpty();

        void print();

        ~Stack();
};

template <typename T>
Stack<T>::Stack() {
    this->head = NULL;
}

template <typename T>
void Stack<T>::push(T value) {
    Node<T>* node = new Node<T>(value);

    node->setNext(head); 
    head = node;
}

template <typename T>
T Stack<T>::pop() {
    T obj = head->getValue();

    Node<T>* r = head;
    head = head->getNext();

    delete r;

    return obj;
}

template <typename T>
bool Stack<T>::isEmpty() {
    return head == NULL;
}

template <typename T>
void Stack<T>::print() {
    Node<T>* anchor = head;

    cout << "Elements: ";

    while(anchor != NULL) {
        cout << anchor->getValue() << " ";
        anchor = anchor->getNext();
    }

    cout << endl;
}

template <typename T>
Stack<T>::~Stack<T>() {
    Node<T> *r; 

    while(head != NULL) {
        r = head;
        head = head->getNext();

        delete r;
    }

    // while(!this->isEmpty()) {
    //     this->pop();
    // }
}

int main() {
    Stack<int>* stack = new Stack<int>();

    stack->push(3);
    stack->push(5);
    stack->push(7);
    stack->push(11);

    stack->print();

    while(!stack->isEmpty()) 
        cout << stack->pop() << endl;

    delete stack;

    return 0;
}