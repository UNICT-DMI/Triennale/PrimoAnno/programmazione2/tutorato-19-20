#include <iostream>

using namespace std;

#define WHITE 0
#define GRAY 1
#define BLACK 2

typedef int color_t;

template <typename T>
class Queue {
    private:
        size_t size;
        size_t head, elements_count;
        T* elements;

    public:
        Queue(size_t);

        void enqueue(T);
        T dequeue();

        bool isEmpty();
        bool isFull();

        void print();

        ~Queue();
};

template <typename T>
Queue<T>::Queue(size_t size) {
    this->size = size;

    this->head = 0;
    this->elements_count = 0;

    this->elements = new T[size];
}

template <typename T>
void Queue<T>::enqueue(T value) {
    elements[(head + elements_count) % size] = value;
    ++elements_count;
}

template <typename T>
T Queue<T>::dequeue() {
    T obj = elements[head];

    --elements_count;
    head = (head + 1) % size;

    return obj;
}

template <typename T>
bool Queue<T>::isEmpty() {
    return elements_count == 0;
}

template <typename T>
bool Queue<T>::isFull() {
    return elements_count == size;
}

template <typename T>
void Queue<T>::print() {
    cout << "| ";

    for(int i = 0; i < elements_count; ++i) {
        cout << elements[(head + i) % size] << " ";
    }

    cout << "| Head: " << head << " Elements count: " << elements_count << endl;
}

template <typename T>
Queue<T>::~Queue() {
    delete elements;
}

template <typename T>
class Node {
    private:
        T value;
        Node<T>* next;

    public:
        Node(T);

        T getValue();

        void setNext(Node*);
        Node* getNext();

        ~Node() { }
};

template <typename T>
Node<T>::Node(T value) {
   this->value = value; 

   this->next = NULL;
}

template <typename T>
T Node<T>::getValue() {
    return this->value;
}

template <typename T>
void Node<T>::setNext(Node<T>* next) {
    this->next = next;
}

template <typename T>
Node<T>* Node<T>::getNext() {
    return this->next;
}

template <typename T>
class List {
    private:
        Node<T>* head;
    
    public:
        List();

        void insert(T);
        void remove(T);
        bool search(T);

        void print();

        ~List();
};

template <typename T>
List<T>::List() {
    this->head = NULL;
}

template <typename T>
void List<T>::insert(T value) {
    Node<T>* node = new Node<T>(value);

    if(head == NULL) {
        head = node;
        return;
    }

    Node<T>* anchor = head;

    while(anchor->getNext() != NULL) {
        anchor = anchor->getNext();
    }

    anchor->setNext(node); 
}

template <typename T>
void List<T>::remove(T value) {
    Node<T>* r;

    if(head->getValue() == value) {
        r = head;
        head = head->getNext();
        delete r;

        return;
    }

    Node<T>* anchor = head;

    while(anchor->getNext() != NULL) {
        if(anchor->getNext()->getValue() == value) {
            r = anchor->getNext();
            anchor->setNext(r->getNext());
            delete r;

            return;
        }

        anchor = anchor->getNext(); 
    }
}

template <typename T>
bool List<T>::search(T value) {
    Node<T>* anchor = head;

    while(anchor != NULL) {
        if(anchor->getValue() == value) {
            return true;
        }

        anchor = anchor->getNext();
    }

    return false;
}

template <typename T>
void List<T>::print() {
    Node<T>* anchor = head;

    cout << "Elements: ";

    while(anchor != NULL) {
        cout << anchor->getValue() << " ";
        anchor = anchor->getNext();
    }

    cout << endl;
}

template <typename T>
List<T>::~List<T>() {
    Node<T> *r; 

    while(head != NULL) {
        r = head;
        head = head->getNext();

        delete r;
    }
}

/***************************************************/

template <typename T>
class Graph {
    private:
        size_t length, N, M;

        T** nodes;
        List<int>** edgesLists;

        int findNodeIndex(T);

        // Per entrambe le visite
        color_t* colors;

        // Per la visita BFS
        int* distances;
        int* predecessors;

        // Per la visita DFS
        uint time;
        uint* discoverTime;
        uint* expirationTime;

        void DFSVisit(int);
    public:
        Graph(size_t);

        Graph<T>* addNode(T);
        Graph<T>* addEdge(T, T);

        void print();

        void BFS(T);
        void DFS();

        ~Graph();
};

template <typename T>
Graph<T>::Graph(size_t length) {
    this->length = length;

    this->N = 0;
    this->M = 0;

    this->nodes = new T*[length];
    this->edgesLists = new List<int>*[length];

    for(size_t i = 0; i < length; ++i) {
        edgesLists[i] = new List<int>();
    }
}

template <typename T>
Graph<T>::~Graph() {
    for(int i = 0; i < N; ++i) {
        delete nodes[i];
    }

    delete nodes;

    for(int i = 0; i < length; ++i) {
        delete edgesLists[i];
    }

    delete edgesLists;
}

template <typename T>
int Graph<T>::findNodeIndex(T value) {
    for(int i = 0; i < N; ++i) {
        if(*(nodes[i]) == value) {
            return i;
        }
    }

    return -1;
}

template <typename T>
Graph<T>* Graph<T>::addNode(T value) {
    int currentIndex = findNodeIndex(value);

    if(currentIndex > 0) {
        cout << "Valore " << value << " già presente (indice " << currentIndex << ")" << endl;
        return this;
    }

    nodes[N] = new T(value);

    ++N;

    return this;
}

template <typename T>
Graph<T>* Graph<T>::addEdge(T firstValue, T secondValue) {
    int firstIndex = findNodeIndex(firstValue);
    int secondIndex = findNodeIndex(secondValue);

    if(firstIndex == -1 || secondIndex == -1) {
        cout << "ERRORE: Valori non presenti nel grafo" << endl;
        return this;
    }

    edgesLists[firstIndex]->insert(secondIndex);

    ++M;

    return this;
}

template <typename T>
void Graph<T>::print() {
    cout << "Numero di nodi: " << N << " | Numero di archi: " << M << endl;

    for(size_t i = 0; i < N; ++i) {
        cout << "[" << i << "] " << *nodes[i] << endl;
    }

    for(size_t i = 0; i < N; ++i) {
        cout << "[" << i << "] ";
        edgesLists[i]->print();
    }
}

template <typename T>
void Graph<T>::BFS(T startValue) {
    if(distances != NULL)
        delete distances;

    if(predecessors!= NULL)
        delete predecessors;

    if(colors != NULL)
        delete colors;

    distances = new int[N];
    predecessors = new int[N];
    colors = new color_t[N];

    for(int i = 0; i < N; ++i) {
        distances[i] = -1;
        predecessors[i] = -1;
        colors[i] = WHITE;
    }

    int startIndex = findNodeIndex(startValue);

    distances[startIndex] = 0;

    if(startIndex == -1) {
        cout << "WARNING: Nodo iniziale non trovato" << endl;
        return;
    }

    Queue<int> visitQueue(N);
    visitQueue.enqueue(startIndex);

    while(!visitQueue.isEmpty()) {
        int i = visitQueue.dequeue();

        colors[i] = GRAY;

        for(int j = 0; j < N; ++j) {
            if(edgesLists[i]->search(*(nodes[j]))) {
                if(colors[j] == WHITE) {
                    visitQueue.enqueue(j);
                    predecessors[j] = i;
                    distances[j] = distances[i] + 1;
                }
            }
        }

        colors[i] = BLACK;
    }

    // Stampiamo i risultati della visita
    for(int i = 0; i < N; ++i) {
        cout << "Distanza di " << *(nodes[i]) << ": " << distances[i];

        if(predecessors[i] >= 0)
            cout << " (passando da " << *(nodes[predecessors[i]]) << ")";

        cout << endl;
    }
}

template <typename T>
void Graph<T>::DFS() {
    time = 0;

    if(discoverTime != NULL)
        delete discoverTime;

    if(expirationTime != NULL)
        delete expirationTime;

    discoverTime = new uint[N];
    expirationTime = new uint[N];
    colors = new color_t[N];

    for(int i = 0; i < N; ++i) {
        colors[i] = WHITE;
    }

    for(int i = 0; i < N; ++i) {
        DFSVisit(i);
    }

    // Stampiamo a schermo i risultati
    for(int i = 0; i < N; ++i) {
        cout << "[" << *(nodes[i]) << "] " << discoverTime[i] << "/" << expirationTime[i] << endl;
    }
}

template <typename T>
void Graph<T>::DFSVisit(int i) {
    if(colors[i] != WHITE)
        return;

    ++time;

    discoverTime[i] = time;

    colors[i] = GRAY;

    for(int j = 0; j < N; ++j) {
        if(edgesLists[i]->search(*(nodes[j])) && colors[j] == WHITE) {
            DFSVisit(j);
        } 
    }

    ++time;
    expirationTime[i] = time;
    colors[i] = BLACK;
}


int main() {
    Graph<char>* G = new Graph<char>(4);

    G->addNode('a')->addNode('b')->addNode('c')->addNode('d');

    G->addEdge('a', 'b');
    G->addEdge('c', 'd');

    G->print();

    cout << "Risultati BFS:" << endl;
    G->BFS('a');

    cout << "Risultati DFS:" << endl;
    G->DFS();

    delete G;

    return 0;
}