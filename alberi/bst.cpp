#include <iostream>

using namespace std;

template <typename T>
class Node {
    private:
        T key;
        Node<T>* left;
        Node<T>* right;

    public:
        Node(T);

        T getKey() { return key; }

        void setLeft(Node<T>* left) { this->left = left; }
        Node<T>* getLeft() { return this->left; }

        void setRight(Node<T>* right) { this->right = right; }
        Node<T>* getRight() { return this->right; }

        ~Node() { }
};

template <typename T>
Node<T>::Node(T key) {
    this->key = key; 
    this->left = NULL;
    this->right = NULL;
}

template <typename T>
class Tree {
    private:
        Node<T>* root;

        Node<T>* searchNodeContaining(T);
        void inOrder(Node<T>*);

        void replant(Node<T>*, Node<T>*, Node<T>*);

        void deallocateNode(Node<T>*);
    public:
        Tree();

        void insert(T);
        void remove(T);
        bool search(T);

        void inOrderPrint();

        ~Tree();
};

template <typename T>
Tree<T>::Tree() {
    this->root = NULL;
}

template <typename T>
void Tree<T>::insert(T value) {
    Node<T>* node = new Node<T>(value);

    if(root == NULL) {
        root = node;
        return;
    }

    Node<T>* anchor = root;

    while(true) {
        if(value > anchor->getKey()) {
            if(anchor->getRight() == NULL) {
                anchor->setRight(node);
                return;
            } else {
                anchor = anchor->getRight();
            }
        } else {
            if(anchor->getLeft() == NULL) {
                anchor->setLeft(node);
                return;
            } else {
                anchor = anchor->getLeft();
            }
        }
    }
}

template <typename T>
Node<T>* Tree<T>::searchNodeContaining(T value) {
    Node<T>* anchor = root;

    while(anchor != NULL) {
        if(value > anchor->getKey()) {
            anchor = anchor->getRight();
        } else if (value < anchor->getKey()){
            anchor = anchor->getLeft();
        } else {
            return anchor;
        }
    }

    return NULL;
}

template <typename T>
bool Tree<T>::search(T value) {
    return this->searchNodeContaining(value) != NULL;
}

template <typename T>
void Tree<T>::replant(Node<T>* parent, Node<T>* node, Node<T>* replacement) {
    if(parent == NULL)
        return;

    if(parent->getLeft() == node) {
        parent->setLeft(replacement);
    } else {
        parent->setRight(replacement);
    }
}

template <typename T>
void Tree<T>::remove(T value) {
    Node<T>* parent = NULL;
    Node<T>* node = root;

    while(node != NULL) {
        if(value > node->getKey()) {
            parent = node;
            node = node->getRight();
        } else if (value < node->getKey()){
            parent = node;
            node = node->getLeft();
        } else {
            break;
        }
    }

    if(node == NULL) {
        // Il valore non è presente nell'albero
        return;
    }
    
    // Caso 1: Il nodo da rimuovere non ha figli
    if(node->getRight() == NULL && node->getLeft() == NULL) {
        replant(parent, node, NULL);

        delete node;
        return;
    }

    // Caso 2.L: Il nodo ha solo il figlio sinistro
    if(node->getLeft() != NULL && node->getRight() == NULL) {
        replant(parent, node, node->getLeft());
        
        delete node;
        return;
    }

    // Caso 2.R: Il nodo ha solo il figlio sinistro
    if(node->getRight() != NULL && node->getLeft() == NULL) {
        replant(parent, node, node->getRight());
        
        delete node;
        return;
    }

    // Caso 3: Il nodo ha due figli
    // Cerchiamo il minimo del sottoalbero destro (successo di 'node')
    Node<T>* successorParent = node;
    Node<T>* successor = node->getRight();

    while(successor->getLeft() != NULL) {
        successorParent = successor;
        successor = successor->getLeft();
    }

    // Se il successore non è il figlio destro di 'node', troviamo nel suo sottoalbero
    if(successor != node->getRight()) {
        // Il figlio destro del successore diventa il figlio sinistro del suo genitore
        // Viene quindi trapiantato sotto il nuovo minimo (dopo la rimozione di 'successor') del sottoalbero destro
        successorParent->setLeft(successor->getRight());

        successor->setRight(node->getRight());
    }

    // Il figlio sinistro di 'node' può diventare direttamente figlio sinistro di 'successor'
    successor->setLeft(node->getLeft());

    replant(parent, node, successor);

    if(node == root) {
        root = successor;
    }

    delete node;
}

template <typename T>
void Tree<T>::inOrder(Node<T>* subtreeRoot) {
    if(subtreeRoot == NULL)
        return;

    inOrder(subtreeRoot->getLeft());
    cout << subtreeRoot->getKey() << " ";
    inOrder(subtreeRoot->getRight());
}

template <typename T>
void Tree<T>::inOrderPrint() {
    this->inOrder(this->root);
    cout << endl;
}

template <typename T>
void Tree<T>::deallocateNode(Node<T>* node) {
    if(node == NULL)
        return;

    if(node->getLeft() != NULL) {
        deallocateNode(node->getLeft());
    }

    if(node->getRight() != NULL) {
        deallocateNode(node->getRight());
    }

    delete node;
}

template <typename T>
Tree<T>::~Tree() {
    deallocateNode(root);
}

int main() {
    Tree<int>* tree = new Tree<int>();

    tree->insert(20);
    tree->insert(3);
    tree->insert(10);
    tree->insert(1);
    tree->insert(0);
    tree->insert(12);
    tree->insert(4);
    tree->insert(6);

    // cout << tree->search(5) << endl;
    // cout << tree->search(8) << endl;
    // tree->inOrderPrint();

    // tree->remove(9);
    // tree->inOrderPrint();

    tree->inOrderPrint();
    tree->remove(3);
    tree->inOrderPrint();

    delete tree;

    return 0;
}