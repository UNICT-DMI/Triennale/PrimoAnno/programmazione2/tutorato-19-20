/* 
Corso di Programmazione II (A-L), Prof. Dario Catalano 

Esercizi su Grafi

Esercizio 4: Algoritmo DFS usando matrici di adiacenza
*/
#include <iostream>
#include <ctime>
#include <math.h>
#include <fstream>
using namespace std;

#define W 0
#define G 1
#define B 2
#define inf len+1


template <class H> class MGraph {
private:
    int len, n, m;
    /* 	n numero di nodi effettivo,
        m numero di archi effettivo,
        len numero massimo di nodi
    */
    int **M; //Matrice di adiacenza
    int t;
    H **K;  // Matrice degli indici
    int *d, *p, *f, *c, *r;
    int current_root;
    //int lastBFSSource;


    int findIndex(H x) {
        // Associa un indice al nodo (se esiste)
        for(int i=0; i<n; i++)
            if(*K[i] == x) return i;
        return -1;
    }

    void printPath(int x) {
        if(x==-1) return;
        if(p[x]==-1) cout << x;
        else {
            printPath(p[x]);
            cout << "->" << x;
        }
    }

public:
    MGraph(int len) {
        this->len = len;
        n = m = 0;
        d = new int[len]; /* vettore delle distanze
								(dalla sorgente) 
								*/
        p = new int[len]; // vettore dei precedenti
        f = new int[len];
        c = new int[len];
        r = new int[len]; // vettore delle radici

        M = new int*[len];
        for(int i=0; i<len; i++) {
            M[i] = new int[len];
            for(int j=0; j<len; j++)
                M[i][j] = 0;
        }
        K = new H*[len];
        for(int i=0; i<len; i++) K[i] = NULL;
    }
    void scambia(int &a, int &b){
        int aux=a;
        a=b;
        b=aux;
    }

    void topologicalSort(ofstream& out){
        DFS();
        H** nodes;
        H* app;
        int* tempi;
        int aux;
        tempi = new int[len];
        nodes = new H*[len];
        for(int i=0;i<len;++i){
            nodes[i]=K[i];
            tempi[i]=f[i];
        }
            //ordinamento
        for(int i=0;i<len-1;i++) {
            for (int j = 0; j < len - i - 1; j++) {
                if (tempi[j] < tempi[j + 1]) {
                    //scambia(tempi[j],tempi[j+1]);
                    aux = tempi[j];
                    tempi[j] = tempi[j + 1];
                    tempi[j+1] = aux;

                    app = nodes[j];
                    nodes[j] = nodes[j + 1];
                    nodes[j+1] = app;
                }
            }
        }

        for(int i=0;i<len;++i){
            out << *nodes[i] << " ";
           // cout << tempi[i] << endl;
        }
        out << endl;
    }

   /*void sort(int *a, int n, int *k) {
       for(int i=1; i<n; i++) {
           int j = i-1;
           while(j>=0 && k[a[j+1]]>k[a[j]]) {
               int t = a[j+1];
               a[j+1] = a[j];
               a[j] = t;
               j--;
           }
       }
   }

   void topSort() {
       DFS();
       int *s = new int[n];
       for(int i=0; i<n; i++) s[i] = i;
       sort(s,n,f);
       for(int i=0; i<n; i++) {
           cout << "(" << *K[s[i]] << ", " << f[s[i]] << ") ";
       }
       cout << endl;
   }*/
    MGraph<H>* addNode(H k) {
        if(n==len) return this;
        if(findIndex(k)>=0) return this;
        K[n] = new H(k);
        n++;
        return this;
    }

    MGraph<H>* addEdge(H x, H y) {
        int i = findIndex(x);
        int j = findIndex(y);
        if(i<0 || j<0) return this;
        if(!M[i][j]) {
            M[i][j] = 1;
            m++;
        }
        return this;
    }

    void print() {
        for(int i=0; i<n; i++) {
            cout << "(" << i << ", " << *K[i] << ")" << " : ";
            for(int j=0; j<n; j++) {
                if(M[i][j]) cout << *K[j] << " ";
            }
            cout << endl;
        }
    }


    void DFSVisit(int v) {
        c[v] = G;
        d[v] = t++;
        r[v] = current_root;
        for(int u=0; u<n; u++) {
            if(M[v][u]==1) {
                if(c[u]==W) {
                    p[u]=v;
                    DFSVisit(u);
                }
            }
        }
        c[v] = B;
        f[v] = t++;
    }

    void DFS() {
        t = 0;
        for(int i=0; i<n; i++) {
            c[i] = W;
            p[i] = -1;
        }
        for(int i=0; i<n; i++)
            if(c[i]==W) {
                current_root = i;
                DFSVisit(i);
            }
    }
};

int main() {

    ifstream input("input.txt");
    ofstream output("output.txt");
    int N, M;
    int vert0;
    int vert1;
    char c_vert0;
    char c_vert1;
    double d_vert0;
    double d_vert1;
    char t;
    string type;

    for(int i=0;i<100;++i){
    input >> N;
    input >> M;
    input >> type;
    if(type=="char"){
        MGraph<char> *Gr = new MGraph<char>(N);
        for(int i=0;i<N;++i){
            input >> c_vert0;
            Gr->addNode(c_vert0);
        }
        for(int i=0;i<M;++i){
            input >> t;
            input >> c_vert0;
            input >> c_vert1;
            input >> t;
            Gr->addEdge(c_vert0,c_vert1);
        }
        Gr->topologicalSort(output);
    }else if(type=="int"){
        MGraph<int> *Gr = new MGraph<int>(N);
        for(int i=0;i<N;++i){
            input >> vert0;
            Gr->addNode(vert0);
        }

        for(int i=0;i<M;++i){
            input >> t;
            input >> vert0;
            input >> vert1;
            input >> t;
            Gr->addEdge(vert0,vert1);
        }
       Gr->topologicalSort(output);

    }else{
        MGraph<double> *Gr = new MGraph<double>(N);
        for(int i=0;i<N;++i){
            input >> d_vert0;
            Gr->addNode(d_vert0);
        }
        for(int i=0;i<M;++i){
            input >> t;
            input >> d_vert0;
            input >> d_vert1;
            input >> t;
            Gr->addEdge(d_vert0,d_vert1);
        }
        Gr->topologicalSort(output);

    }
    }


}