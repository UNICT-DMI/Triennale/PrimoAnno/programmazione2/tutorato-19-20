// Autore: Simone Costanzo

#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
using namespace std;

class lista
{
public:
    lista(int n)
    {
        firstFreeIndex = 0;
        K = new int[n];
        N = new int[n];
        H = -1;
    };
    void stampa(ofstream &out);
    void inserisci(int val);

private:
    int successivo(int val);
    int firstFreeIndex;
    int H;
    int *K;
    int *N;
};

int lista::successivo(int x)
{
    int indx;
    for (int i = 0; i < firstFreeIndex; i++)
    {
        int max = K[0];
        for (int i = 0; i < firstFreeIndex; i++)
        {
            if (K[i] > max)
            {
                max = K[i];
            }
        }
        if (K[i] >= K[x] && K[i] <= max)
        {
            indx = i;
            max = K[i];
        }
    }
    return indx;
}

void lista::inserisci(int val)
{
    //cout << "Inserisco " << val << ':' << endl;
    K[firstFreeIndex] = val;
    if (H == -1)
    {
        H = 0;
        N[H] = -1;
        //cout << "La lista Ã¨ vuota" << endl;
    }
    else if (val < K[H])
    {
        N[firstFreeIndex] = H;
        H = firstFreeIndex;
        //cout << "Inserito il nuovo minimo: " << H << endl;
    }
    else
    {
        int index = H;
        int indSucc = N[index];
        while (indSucc > -1 && val >= K[indSucc])
        {
            index = indSucc;
            indSucc = N[index];
        }
        //cout << "index: " << index << " indSucc:" << indSucc << endl;
        //int saro = firstFreeIndex; rip saro ;(
        N[index] = firstFreeIndex;
        N[firstFreeIndex] = indSucc;
    }
    firstFreeIndex++;
}

void lista::stampa(ofstream &out)
{
    for (int i = 0; i < firstFreeIndex; i++)
    {
        out << N[i] << " ";
    }
}

int main()
{

    ifstream input;
    ofstream output;

    input.open("input.txt");
    output.open("output.txt");

    for (int task = 1; task <= 100; task++)
    {
        int n;
        input >> n;
        lista lista(n);
        for (int i = 0; i < n; i++)
        {
            int num;
            input >> num;
            lista.inserisci(num);
        }
        lista.stampa(output);
        output << endl;
    }
    return 0;
}