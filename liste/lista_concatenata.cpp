// Esercizio 1: Implementare la lista in modo che abbia un puntatore alla coda, ottimizzando l'inserimento.

#include <iostream>

using namespace std;

template <typename T>
class Node {
    private:
        T value;
        Node<T>* next;

    public:
        Node(T);

        T getValue();

        void setNext(Node*);
        Node* getNext();

        ~Node() { }
};

template <typename T>
Node<T>::Node(T value) {
   this->value = value; 

   this->next = NULL;
}

template <typename T>
T Node<T>::getValue() {
    return this->value;
}

template <typename T>
void Node<T>::setNext(Node<T>* next) {
    this->next = next;
}

template <typename T>
Node<T>* Node<T>::getNext() {
    return this->next;
}

template <typename T>
class List {
    private:
        Node<T>* head;
    
    public:
        List();

        void insert(T);
        void remove(T);
        bool search(T);

        void print();

        ~List();
};

template <typename T>
List<T>::List() {
    this->head = NULL;
}

template <typename T>
void List<T>::insert(T value) {
    Node<T>* node = new Node<T>(value);

    if(head == NULL) {
        head = node;
        return;
    }

    Node<T>* anchor = head;

    while(anchor->getNext() != NULL) {
        anchor = anchor->getNext();
    }

    anchor->setNext(node); 
}

template <typename T>
void List<T>::remove(T value) {
    Node<T>* r;

    if(head->getValue() == value) {
        r = head;
        head = head->getNext();
        delete r;

        return;
    }

    Node<T>* anchor = head;

    while(anchor->getNext() != NULL) {
        if(anchor->getNext()->getValue() == value) {
            r = anchor->getNext();
            anchor->setNext(r->getNext());
            delete r;

            return;
        }

        anchor = anchor->getNext(); 
    }
}

template <typename T>
bool List<T>::search(T value) {
    Node<T>* anchor = head;

    while(anchor != NULL) {
        if(anchor->getValue() == value) {
            return true;
        }

        anchor = anchor->getNext();
    }

    return false;
}

template <typename T>
void List<T>::print() {
    Node<T>* anchor = head;

    cout << "Elements: ";

    while(anchor != NULL) {
        cout << anchor->getValue() << " ";
        anchor = anchor->getNext();
    }

    cout << endl;
}

template <typename T>
List<T>::~List<T>() {
    Node<T> *r; 

    while(head != NULL) {
        r = head;
        head = head->getNext();

        delete r;
    }
}

int main() {
    List<int>* list = new List<int>();

    list->insert(1);

    list->insert(5);
    list->insert(7);

    list->print();

    list->remove(8);

    list->print();
    
    list->insert(10);
    list->insert(12);

    list->print();

    cout << list->search(5) << endl;
    cout << list->search(10) << endl;
    cout << list->search(15) << endl;

    delete list;

    return 0;
}