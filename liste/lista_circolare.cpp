// Esercizio 1: Implementare la lista in modo che abbia un puntatore alla coda, ottimizzando l'inserimento. Applicare altre varie ottimizzazioni che sapere qual'è il nodo di coda ci permetterebbe di fare.

#include <iostream>

using namespace std;

class Node {
    private:
        int value;
        Node* next;

    public:
        Node(int);

        int getValue();

        void setNext(Node*);
        Node* getNext();

        ~Node() { }
};

Node::Node(int value) {
   this->value = value; 

   this->next = NULL;
}

int Node::getValue() {
    return this->value;
}

void Node::setNext(Node* next) {
    this->next = next;
}

Node* Node::getNext() {
    return this->next;
}

class List {
    private:
        Node* head;
    
    public:
        List();

        void insert(int);
        void remove(int);
        bool search(int);

        void print();

        ~List();
};

List::List() {
    this->head = NULL;
}

void List::insert(int value) {
    Node* node = new Node(value);

    if(head == NULL) {
        head = node;
    } else {
        Node* anchor = head;

        while(anchor->getNext() != head) {
            anchor = anchor->getNext();
        }

        anchor->setNext(node); 
    }

    node->setNext(head);
}

void List::remove(int value) {
    Node* r;

    if(head->getValue() == value) {
        r = head;

        if(head->getNext() != head) {
            head = head->getNext();
        } else {
            head = NULL;
        }

        delete r;

        return;
    }

    Node* anchor = head;

    while(anchor->getNext() != head) {
        if(anchor->getNext()->getValue() == value) {
            r = anchor->getNext();
            anchor->setNext(r->getNext());
            delete r;

            return;
        }

        anchor = anchor->getNext(); 
    }
}

bool List::search(int value) {
    if(head == NULL)
        return false;

    Node* anchor = head;

    do {
        if(anchor->getValue() == value) {
            return true;
        }

        anchor = anchor->getNext();
    } while(anchor != head);

    return false;
}

void List::print() {
    if(head == NULL) {
        return;
    }

    Node* anchor = head;

    cout << "Elements: ";

    do {
        cout << anchor->getValue() << " ";
        anchor = anchor->getNext();
    } while(anchor != head);

    cout << endl;
}

List::~List() {
    if(head == NULL)
        return;

    Node *r, *anchor; 

    anchor = head;

    do {
        r = anchor;
        anchor = anchor->getNext();

        delete r;
    } while(anchor != head);
}

int main() {
    List* list = new List();

    list->insert(1);
    list->insert(5);
    list->insert(7);

    list->print();

    list->remove(8);

    list->print();
    
    list->insert(10);
    list->insert(12);

    list->print();

    cout << list->search(5) << endl;
    cout << list->search(10) << endl;
    cout << list->search(15) << endl;

    delete list;

    return 0;
}