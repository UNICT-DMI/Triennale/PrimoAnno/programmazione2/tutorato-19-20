// Esercizio 1: Implementare la lista in modo che abbia un puntatore alla coda, ottimizzando l'inserimento.

// Esercizio 2: Implementare la variante circolare della lista doppiamento concatenata

#include <iostream>

using namespace std;

class Node {
    private:
        int value;
        Node* prev;
        Node* next;

    public:
        Node(int);

        int getValue();

        void setNext(Node*);
        Node* getNext();

        void setPrev(Node*);
        Node* getPrev();

        ~Node() { }
};

Node::Node(int value) {
   this->value = value; 

   this->next = NULL;
   this->prev = NULL;
}

int Node::getValue() {
    return this->value;
}

void Node::setNext(Node* next) {
    this->next = next;
}

Node* Node::getNext() {
    return this->next;
}

void Node::setPrev(Node* prev) {
    this->prev = prev;
}

Node* Node::getPrev() {
    return this->prev;
}

class List {
    private:
        Node* head;
    
    public:
        List();

        void insert(int);
        void remove(int);
        bool search(int);

        void print();

        ~List();
};

List::List() {
    this->head = NULL;
}

void List::insert(int value) {
    Node* node = new Node(value);

    if(head == NULL) {
        head = node;
        return;
    }

    Node* anchor = head;

    while(anchor->getNext() != NULL) {
        anchor = anchor->getNext();
    }

    anchor->setNext(node); 
    node->setPrev(anchor);
}

void List::remove(int value) {
    Node* r;

    if(head->getValue() == value) {
        r = head;
        head = r->getNext();
        delete r;

        head->setPrev(NULL);

        return;
    }

    Node* anchor = head;

    while(anchor->getNext() != NULL) {
        if(anchor->getNext()->getValue() == value) {
            r = anchor->getNext();
            anchor->setNext(r->getNext());
            delete r;

            anchor->getNext()->setNext(anchor);

            return;
        }

        anchor = anchor->getNext(); 
    }
}

bool List::search(int value) {
    Node* anchor = head;

    while(anchor != NULL) {
        if(anchor->getValue() == value) {
            return true;
        }

        anchor = anchor->getNext();
    }

    return false;
}

void List::print() {
    Node* anchor = head;

    cout << "Elements: ";

    while(anchor != NULL) {
        cout << anchor->getValue() << " ";
        anchor = anchor->getNext();
    }

    cout << endl;
}

List::~List() {
    Node *r; 

    while(head != NULL) {
        r = head;
        head = head->getNext();

        delete r;
    }
}

int main() {
    List* list = new List();

    list->insert(1);

    list->insert(5);
    list->insert(7);

    list->print();

    list->remove(8);

    list->print();
    
    list->insert(10);
    list->insert(12);

    list->print();

    cout << list->search(5) << endl;
    cout << list->search(10) << endl;
    cout << list->search(15) << endl;

    delete list;

    return 0;
}