#include<iostream>
#include<cstdlib>

using namespace std;

void bubbleSort(int *array, int n){
    int cont=0;
    for(int i=0; i<n-1; i++){
        for(int j=0; j<n-1; j++){
            cont++;
            if(array[j] > array[j+1]){
                int temp = array[j];
                array[j] = array[j+1];
                array[j+1] = temp;
            }
        }
    }
    cout << "Sono state eseguite " << cont << " operazioni" << endl;
}

void bubbleSortSentinella(int *array, int n){
    bool check=true;
    int cont=0;
    for(int i=0; i<n-1 && check; i++){
        check=false;
        for(int j=0; j<n-1; j++){
            cont++;
            if(array[j] > array[j+1]){
                int temp = array[j];
                array[j] = array[j+1];
                array[j+1] = temp;
                check=true;
            }
        }
    }
    cout << "Sono state eseguite " << cont << " operazioni" << endl;
}

void selectionSort(int *array, int n){
    int count=0;
    for(int i=0; i<n-1; i++){
        for(int j=i+1; j<n; j++){
            count++;
            if(array[i] > array[j]){
                int temp = array[j];
                array[j] = array[i];
                array[i] = temp;
            }
        }
    }
    cout << "Sono state eseguite " << count << " operazioni" << endl;
}

void merge(int* array, int l, int m, int r) {
    int leftSize = m-l+1;
    int rightSize = r-m;

    int L[leftSize], R[rightSize];

    for(int i = 0; i < leftSize; ++i)
        L[i] = array[l + i];

    for(int i = 0; i < rightSize; ++i)
        R[i] = array[m + 1 + i];

    int leftMinIndex = 0, rightMinIndex = 0; 

    int arrayIndex = l;

    while(leftMinIndex < leftSize && rightMinIndex < rightSize) {
        if(L[leftMinIndex] < R[rightMinIndex]) {
            array[arrayIndex] = L[leftMinIndex];
            ++leftMinIndex;
        } else {
            array[arrayIndex] = R[rightMinIndex];
            ++rightMinIndex;
        }

        ++arrayIndex;
    }

    while(leftMinIndex < leftSize) {
        array[arrayIndex] = L[leftMinIndex];
        ++leftMinIndex;
        ++arrayIndex;
    }

    while(rightMinIndex < rightSize) {
        array[arrayIndex] = R[rightMinIndex];
        ++rightMinIndex;
        ++arrayIndex;
    }
}

void mergeSort(int* array, int l, int r) {
    if(l >= r) {
        return;
    }

    if(l == r-1) {
        if(array[l] > array[r]) {
            swap(array[l], array[r]);
        }
        return;
    }

    int size = r - l + 1;
    int m = l + size/2;

    mergeSort(array, l, m);
    mergeSort(array, m+1, r);

    merge(array, l, m, r);
}

void mergeSort(int* array, int n) {
    mergeSort(array, 0, n-1);
}

void insertionSort(int *array, int n){
    int min;
    int count=0;
    for(int i=1; i<n; i++){
        min = array[i];
        int j=i-1;

        while(j>=0 && array[j] > min){
            count++;
            array[j+1] = array[j];
            j=j-1;
        }
        array[j+1] = min;
        count++;
    }
    cout << "Sono state eseguite " << count << " operazioni" << endl;

}

int partition(int* array, int l, int r, int p) {
    int i = l - 1;
    int j = l;

    while(j <= r) {
        if(array[j] < array[p]) {
            ++i;

            swap(array[i], array[j]);
        }

        ++j;
    }

    swap(array[i+1], array[p]);

    return i+1;
}

void quickSort(int* array, int l, int r) {
    if(l >= r) {
        return;
    }

    if(l == r-1) {
        if(array[l] > array[r]) {
            swap(array[l], array[r]);
        }
        return;
    }

    int p = partition(array, l, r-1, r);

    quickSort(array, l, p-1);
    quickSort(array, p+1, r);
}

void quickSort(int* array, int n) {
    quickSort(array, 0, n-1);
}

int main(){
    srand(time(0));

    int n=100, cont=0;

    int array[n];

    for(int i=0; i<n; i++){
        array[i] = rand()%1000;
        cout << array[i] << " ";
        //cont++;
    }
    cout << endl;
    
    //bubbleSort(array, n);
    //bubbleSort(array, n);
    //bubbleSortSentinella(array, n);
    //insertionSort(array, n);
    //mergeSort(array, n);
    quickSort(array, n);

    for(int i=0; i<n; i++){
        cout << array[i] << " ";

        if(i < n - 1 && array[i] > array[i+1]) {
            cout << "ERRORE ALL'INDICE " << i << endl;
        }
    }
    cout << endl;

    //cout << cont << endl;
}